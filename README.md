# GC_Analytics

## For gc-fit.ipynb

Check `Ar_200microlH2_200microlCH4.txt` for data format.

Execute:
```
import sys
!{sys.executable} -m pip install lmfit

import sys
!{sys.executable} -m pip install tabulate
```

## For regression.ipynb

Check `values.txt` for data format.

Execute:
```
import sys
!{sys.executable} -m pip install uncertainties
```